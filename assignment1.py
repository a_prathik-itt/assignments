"""
The below program is to compare if two complex dictionaries are equal or not.
dictCmp functions takes 2 parameters which is of type dictionary to compare. 
The parameter a is considered as the reference dictionary

Keywords
dict - dictionary

"""


def dictCmp(a,b):

    # key1 holds the keys for the dictionary a which is our refrence dict
    key1 = list(a.keys())
    # key2 = list(b.keys())

    # Flag condition to check if the dictionary is equal or not
    f = True

    # Printing dictionary a for reference
    print("\nDictionary:",a)

    #Iterating with respect to dict a
    for k in key1:

        print("\nKey:",k)

        # Condition to check if the value present has another dict inside
        if(b.get(k)!=None and type(a[k])==type(b[k]) and type(a[k])==dict):
            print("\nEntered dictCmp part")
            # Recursively passing the values if it is a dict
            dictCmp(a[k],b[k])


        # Condition to check if the key present in dict a is also present in dict b
        elif(b.get(k)==None):
            print("\nEntered b.get(k)")
            print("Difference in Dictionary B at ",k)            
            print("Key:",k,"\tValue:",a[k])

        # Condition where we confirm that the dictionary passed has key:value pairs where the value is not a dict

        else:
            print("\nEntered else")
            # for k1 in key1:
            if(a[k]!=b[k]):
                f = False

            print("\nA:",a[k],"B",b[k])

            #     else:
            #         f = False
            #         print("\nDifference in Dictionary B at ",k1)            
            #         print("Key of A:",k1,"\tValue of A:",a[k1],"\tType of A:",type(a))
            #         print("Key of B:",k1,"\tValue of B:",b[k1],"\tType of B:",type(b))

    if(f):
        print("\nBoth the dictionaries are same with respect to:",k)

    else:
        print("\nDifferent dictionary")



                    
a={"std1":{"name":"ABC","age":20,"marks":10},"std2":{"name":"EFG","age":30,"marks":10}}
b={"std2":{"name":"EFG","age":30,"marks":10},"std1":{"name":"ABC","age":20,"marks":20}}
dictCmp(a,b)